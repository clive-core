# clive licensing

- clive core code is under dual BSD3 and GPL3 license (master branch)
- clive documentation is under CC-BY-SA license (master branch)
- Claude's clive performances are under GPL3 license (other branches)
