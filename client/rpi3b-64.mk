GCC ?= aarch64-linux-gnu-gcc-8

../build/go.so: go.c dsp.h dsp/*.h
	$(GCC) -std=c99 -Wall -pedantic -Wextra -Wno-unused-parameter -O3 -march=armv8-a+crc -I. -shared -fPIC -o ../build/go.so go.c -lm
