#include "go.h"

int go(S *s, int channels, const float *in, float *out) {
  if (s->reloaded) {
    s->reloaded = 0;
  }
  for (int c = 0; c < channels; ++c) {
    out[c] = 0;
  }
  return 0;
}
