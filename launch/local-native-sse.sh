#!/bin/bash
for c in $(cat /proc/cpuinfo | grep "^processor" | sed "s/processor\t: //")
do
  sudo cpufreq-set -c "${c}" -g performance
done
make -C client
make -C server
mkdir -p build
SESSION="session-$(date -u +%F-%H%M%S)"
git checkout -b "${SESSION}"
(
ecasound -q -G:jack,record -f:f32,2,48000 -i:jack -o "${SESSION}.wav" &
sleep 5
xterm -geometry 80x10+0+0   -T server -e bash -c 'cd server ; while true ; do ./clive-server ; done' &
xterm -geometry 80x30+0+154 -T client -e bash -c 'cd client ; while true ; do ./clive-client native-sse.mk ; done' &
xterm -geometry 80x8+0+588  -T htop   -e bash -c 'while true ; do htop ; done' &
{ cd client ; while true ; do geany -mist dsp.h dsp/*.h go.c ; done } &
wait
)
